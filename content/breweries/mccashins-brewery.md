---
title: McCashin's Brewery
tags: ["mccashins brewery","brewery","nelson","restaurant"]
type: brewery
lat: -41.3144788
long: 173.229147
address: ["660 Main Rd","Stoke","Nelson 7011","New Zealand"]
phone: 
  1: ["+64 03 547 5357",""]
hours: 
  1: ["Tours (Bookings Essential):","11am Mon-Fri"]
  2: ["Tours (Bookings Essential):","2pm Wed and Fri only"]
  3: ["",""]
  4: ["Kitchen:","7.00am to 6.00pm Mon-Wed"]
  5: ["Kitchen:","7.00am to 10.00pm Thurs-Fri"]
  6: ["Kitchen:","9.00am to 10.00pm Sat"]
  7: ["Kitchen:","9.00am to 8.00pm Sun"] 
website: http://www.mccashins.co.nz/
email: info@mccashins.co.nz
facebook: https://www.facebook.com/McCashinsBrewery/
twitter: https://twitter.com/Stokebeer
instagram: https://www.instagram.com/mccashinsbrewery/
pintest: 
---
{{< comment >}}description{{< /comment >}}
The McCashin Family Brewery and Distillery is located in Stoke, Nelson, at the top of the South Island, New Zealand. The region is renowned for its Sunshine, hop growing and more recently for its world class wineries and boutique breweries.

The McCashin Family now produce a variety of quality products, using locally produced ingredients of the highest quality.

{{< load-hours >}}
{{< load-phones >}}
{{< load-leaflet >}}

{{< comment >}}image gallery{{< /comment >}}
#### Photos
{{< gallery caption-effect="fade" >}}
  {{< figure link="/img/breweries/2019-02-21-maccashins-1.jpg" >}}
{{< /gallery >}}
{{< load-photoswipe >}}