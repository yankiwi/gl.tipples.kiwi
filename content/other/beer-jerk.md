---
title: Beer Jerk
tags: ["beer jerk","bar","subscription","auckland"]
type: other
lat: -36.8646556
long: 174.7609616
address: ["Basement 224 Symonds St","Eden Terrace","Auckland 1010 New Zealand"]
hours: 
  1: ["Mon","Closed"]
  2: ["Tue","Closed"]
  3: ["Wed","5.00pm to at least 10.00pm"]
  4: ["Thurs","5.00pm to at least 10.00pm"]
  5: ["Fri","4.00pm to at least 11.00pm"]
  6: ["Sat","3.00pm to at least 11.00pm"]
  7: ["Sun","3.00pm to at least 8.00pm"]
website: https://www.beerjerk.co.nz/
email: contact@beerjerk.co.nz
facebook: https://www.facebook.com/thebeerjerk/
twitter: https://twitter.com/BeerJerkNZ/
instagram: https://www.instagram.com/beerjerknz/
pintest: 
---
{{< comment >}}description{{< /comment >}}
A bit like a book club for wonderful rare beers. And no books. Members receive a box of truly unique beers every 12 weeks. Never drink the same beer twice.

And also, the Beer Jerk Bunker.  Stocking rare, interesting or hard to find beers from around New Zealand and the world and provide customers a great place to drink beers with mates.

{{< load-hours >}}
{{< load-phones >}}
{{< load-leaflet >}}
