---
title: Plume Vineyard Restaurant
tags: ["plume","restaurant","auckland","matakana","cellar door","restaurant","accommodations"]
type: other
lat: -36.376240
long: 174.704060
address: ["49A Sharp Rd, Matakana","Auckland 0982, New Zealand"]
phone: 
  1: ["+64 09 422 7915",""]
hours: 
  1: ["11.00am to 2.00pm","Wed-Fri"]
  2: ["12.00am to 3:00pm","Sat-Sun"]
website: 
email: contact@plumerestaurant.co.nz
facebook: https://www.facebook.com/plumerestaurant/
twitter: 
instagram: https://www.instagram.com/plumenz/
pintest: 
---
{{< comment >}}description{{< /comment >}}
Situated just an hour’s drive from Auckland or Whangarei, Plume, the vineyard restaurant, Matakana is a must visit destination on any Matakana wine or food journey.  Plume is all about fabulous local New Zealand wine and produce which together can celebrate "culinary diversity."  Plume is a small town, rural, rustic restaurant with friendly service from locally employed staff.

{{< load-hours >}}
{{< load-phones >}}
{{< load-leaflet >}}
