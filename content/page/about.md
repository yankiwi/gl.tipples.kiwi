---
title: About
subtitle: tipples.kiwi
comments: false
---

Thank you for using [tipples.kiwi](https://tipples.kiwi) - A mobile app and website to help New Zealand locals and visitors find and enjoy Breweries, Wineries, Vineyards, Distilleries and more.

For information, additions or correction just contact us via the links below!