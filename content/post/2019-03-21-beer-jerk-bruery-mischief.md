---
title: Beer Jerk - The Bruery Mischief
date: 2019-02-21
tags: ["beer jerk","beer","brewery","tasting"]
---

We flew down to Westport today for the [Old Ghost Ultra](http://www.oldghostultra.com/) run this weekend. I had to stash this week's [Beer Jerk](other/beer-jerk/) beer inside of a trail running shoe and serve it in hotel glassware but it was all worth it. The Bruery's Mischief Belgian-style Ale turned out to be exactly the right combination of refreshing fruity hoppiness after a long day of travelling. #BeerJerkNZ

{{< gallery caption-effect="fade" >}}
  {{< figure link="/img/blog/2019-02-21-beer-jerk-1.jpg" >}}
{{< /gallery >}}
