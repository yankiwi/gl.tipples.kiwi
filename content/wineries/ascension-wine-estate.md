---
title: Ascension Wine Estate
tags: ["ascension wine","winery","auckland","matakana","cellar door","restaurant"]
type: winery
lat: -36.3728748
long: 174.6783208
address: ["480 Matakana Rd","Laly Haddon Place","Warkworth 0985, New Zealand"]
phone: 
  1: ["+64 09 422 9601",""]
hours: 
  1: ["Cellar Door 11.00am to 4.00pm","7 days"]
  2: ["Restaurant","1-Nov to 30-Apr"]
website: http://ascensionwine.co.nz/
email: info@ascensionwine.co.nz
facebook: https://www.facebook.com/AscensionWine/
twitter: https://twitter.com/AscensionWine/
instagram: https://instagram.com/ascensionwineestate/
pintest: https://nz.pinterest.com/ascensionwine/
---
{{< comment >}}description{{< /comment >}}
Ascension Wine Estate offers a selection of handcrafted wine in a scenic location just 45 minutes north of Auckland in the hills of Matakana.  Enjoy the rural scenery around their lovely estate where you'll find cellar door winetastings, a restaurant and gardens.


{{< load-hours >}}
{{< load-phones >}}
{{< load-leaflet >}}
