---
title: Awhitu Wines
tags: ["awhitu","winery","auckland","cellar door"]
type: winery
lat: -37.060470
long: 174.663510
address: ["31 Greenock Drive","Awhitu Peninsula","Auckland, New Zealand"]
phone: 
  1: ["+64 09 235 1465",""]
hours: 
  1: ["10.00am to 5.00pm",""]
website: http://www.awhituwines.co.nz/
facebook: 
twitter: 
instagram: 
pintest: 
---
{{< comment >}}description{{< /comment >}}
Just an hour and a half from Auckland, Awhitu Wines is found near Grahams Beach on the Awhitu Peninsula.  This small high quality vineyard offers an elegant, hand-crafted selection of Chardonnay, Rose, and Syrah.

{{< load-hours >}}
{{< load-phones >}}
{{< load-leaflet >}}

{{< comment >}}image gallery{{< /comment >}}
#### Photos
{{< gallery caption-effect="fade" >}}
  {{< figure link="/img/blog/2018-08-25-winery-1.jpg" >}}
  {{< figure link="/img/blog/2018-08-25-winery-2.jpg" >}}
  {{< figure link="/img/blog/2018-08-25-winery-3.jpg" >}}
  {{< figure link="/img/blog/2018-08-25-winery-4.jpg" >}}
  {{< figure link="/img/blog/2018-08-25-winery-5.jpg" >}}
  {{< figure link="/img/blog/2018-08-25-winery-6.jpg" >}}
{{< /gallery >}}
{{< load-photoswipe >}}
