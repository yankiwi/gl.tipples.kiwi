---
title: Brick Bay
tags: ["cable bay","winery","auckland","cellar door","restaurant"]
type: winery
lat: -36.407751
long: 174.720834
address: ["17 Arabella Lane","Snells Beach 0920 Warkworth, Auckland","New Zealand"]
phone: 
  1: ["+64 09 425 4690",""]
hours: 
  1: ["10.00am to 5.00pm","Daily"]
website: https://www.brickbay.co.nz/
email: glasshouse@brickbay.co.nz
facebook: https://www.facebook.com/brickbay/
twitter: 
instagram: https://www.instagram.com/brick_bay/
pintest: 
---
{{< comment >}}description{{< /comment >}}
Situated on the coast in the Matakana Wine Region, our 200 acres hosts a boutique vineyard, an extensive outdoor sculpture trail, an architectural-award winning restaurant, in the midst of an idyllic landscape of farmed pasture and native bush.

{{< load-hours >}}
{{< load-phones >}}
{{< load-leaflet >}}
