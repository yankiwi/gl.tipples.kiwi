---
title: Cable Bay Vineyards
tags: ["cable bay","winery","auckland","waiheke","cellar door","restaurant"]
type: winery
lat: -36.7876947
long: 174.9981793
address: ["12 Nick Johnstone Drive","Oneroa, Waiheke Island","Auckland 1840, New Zealand"]
phone: 
  1: ["+64 09 372 5889",""]
hours: 
  1: ["Cellar Door 11.00am to 5.00pm","Daily"]
  2: ["Lunch from 12.00pm","Thurs-Sun"]
  3: ["Dinner from 6.00pm","Fri-Sat"]
website: https://cablebay.nz/
email: info@cablebay.co.nz
facebook: https://www.facebook.com/Cable.Bay.Vineyards/
twitter: 
instagram: https://www.instagram.com/cable_bay_vineyards/
pintest: 
---
{{< comment >}}description{{< /comment >}}
Cable Bay Vineyard is location on beautiful Waiheke Island, just a short ferry ride from Auckland CBD.

At Cable Bay you'll find handcrafted wines from high quality fruit grown in small batch vineyards on Waiheke and in Marlborough.  With award winning wines at their cellar door and exquisite cuisine in the Dining Room, relaxed dining in The Verandah and great views all around, Cable Bay is a must on your next Waiheke Island trip.

{{< load-hours >}}
{{< load-phones >}}
{{< load-leaflet >}}

