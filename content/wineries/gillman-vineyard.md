---
title: Gillman Vineyard
tags: ["gillman vineyard","winery","auckland"]
type: winery
lat: -36.372440
long: 174.703030
address: ["99 Sharp Road","Matakana, 0982, New Zealand"]
phone: 
  1: ["+64 21 037 3445",""]
hours: 
  1: ["by appointment only",""]
website: http://www.gillmanvineyard.co.nz/
email: toby@gillmanvineyard.co.nz
facebook: 
twitter: 
instagram: 
pintest: 
---
{{< comment >}}description{{< /comment >}}
As great wine can only be made from the highest quality grapes, they searched for two years to find the perfect place to establish Gillman Vineyard. The Matakana region, only an hour’s drive north of Auckland, has warm summers and low rainfall to create ideal conditions for traditional Bordeaux varieties.

{{< load-hours >}}
{{< load-phones >}}
{{< load-leaflet >}}
