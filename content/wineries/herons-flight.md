---
title: Heron's Flight
tags: ["herons flight","winery","auckland","matakana","cellar door"]
type: winery
lat: -36.372440
long: 174.703030
address: ["49 Sharp Rd","Matakana, Auckland, New Zealand"]
phone: 
  1: ["+64 09 950 6643",""]
hours: 
  1: ["10.00am - 5.00pm ","Daily"]
website: https://www.heronsflight.co.nz/
email: contact@heronsflight.co.nz
facebook: https://www.facebook.com/heronsflight.co.nz/
twitter: https://twitter.com/heronsflightnz
instagram: https://www.instagram.com/heronsflight/
pintest: 
---
{{< comment >}}description{{< /comment >}}
Heron's Flight is a small family owned vineyard in Matakana, Auckland North - famous for its beautiful beaches and boutique wineries.

They specialize in growing the Italian grape varieties Sangiovese and Dolcetto, which we have successfully grown since 1993. From these grapes we make many products, including wine, grapejuice and verjus.

{{< load-hours >}}
{{< load-phones >}}
{{< load-leaflet >}}
