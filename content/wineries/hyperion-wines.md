---
title: Hyperion Wines
tags: ["hyperion","winery","auckland","matakana","cellar door"]
type: winery
lat: -36.359520
long: 174.732010
address: ["188 Tongue Farm Rd","Matakana 0986, New Zealand"]
phone: 
  1: ["+64 09 422 9375",""]
hours: 
  1: ["11.00am to 4.00pm","daily in January"]
  2: ["11.00am to 4.00pm","weekends and holiday during summer months"]
  3: ["by appointment","winter months"]
website: https://www.hyperionwines.co.nz/
email: info@hyperionwines.co.nz
facebook: https://www.facebook.com/pg/Hyperion-Wines-139521169416719/
twitter: 
instagram: 
pintest: 
---
{{< comment >}}description{{< /comment >}}
Hyperion Wines is a genuine boutique vineyard and winery producing traditionally crafted wines exclusively from the Matakana winegrowing region, just an hour north of Auckland New Zealand.

This small home vineyard plants a classic grape varieties of Cabernet Sauvignon, Malbec, Pinot Noir and Chardonnay, plus the exciting new hydrid Chambourcinand soucres Pinot Gris and Syrah from other Matakana vineyards. All processing including bottling is performed on site in a character converted cowshed. Port and Grappa are also made on the premises.

{{< load-hours >}}
{{< load-phones >}}
{{< load-leaflet >}}
