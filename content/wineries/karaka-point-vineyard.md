---
title: Karaka Point Vineyard & Lodge
tags: ["karaka point","winery","auckland","accommodations"]
type: winery
lat: -37.1396968
long: 174.73506
address: ["35C Wallace Drive","Clarks Beach","Auckland Central","Auckland 2679, New Zealand"]
phone: 
  1: ["+64 021 966 058",""]
hours: 
  1: ["Cellar Door","by appointment only"]
  2: ["Office","9.00am to 5.00pm Mon-Fri"]
website: http://www.karakapointvineyard.co.nz
email: info@karakapointvineyard.co.nz
facebook: 
twitter: 
instagram: 
pintest: 
---
{{< comment >}}description{{< /comment >}}
Karaka Point Vineyard & Lodge is located just 30 minutes southwest of Auckland near Clark's Beach.  Karaka Point offers a selection of hand-crafted Chardonnay, Syrah, Pinot Noir and Rose and while the cellar door is sadly open by appointment only, this beautiful vineyard does offer European inspired acommodations and event hosting.


{{< load-hours >}}
{{< load-phones >}}
{{< load-leaflet >}}
