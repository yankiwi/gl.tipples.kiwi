---
title: Lothlorien Winery
tags: ["lothlorien","winery","auckland"]
type: winery
lat: -36.479474
long: 174.5590313
address: ["1227 Ahuroa Road RD1","Warkworth 0981, New Zealand"]
phone: 
  1: ["+64 09 422 5846",""]
hours: 
  1: ["9.00am to 4.30pm","Mon-Fri"]
  2: ["by appointment","Sat-Sun"]
website: http://www.lothlorienwinery.co.nz/
email: info@lothlorienwinery.co.nz
facebook: https://www.facebook.com/lothlorienfeijoawine/
twitter: https://twitter.com/lothfeijoawine
instagram: https://www.instagram.com/lothlorienfeijoawine/
pintest: 
---
{{< comment >}}description{{< /comment >}}
Lothlorien Winery produces award winning wine, liqueur and fruit juices. Their well tended orchards have gone through the official organic certification process and juicing and winemaking are done onsite.  Lothlorien are the only certified organic feijoa winery in New Zealand and possibly the world.


{{< load-hours >}}
{{< load-phones >}}
{{< load-leaflet >}}
