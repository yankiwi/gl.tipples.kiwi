---
title: Matakana Estate
tags: ["matakana","winery","auckland","cellar door"]
type: winery
lat: -36.372913
long: 174.6843833
address: ["68 Matakana Road","Matakana","Auckland, New Zealand 0948"]
phone: 
  1: ["+64 0800 568 686"]
  2: ["+64 9 425 0105"]
hours: 
  1: ["9.00am to 5.00pm","Mon-Fri"]
website: http://matakanaestate.co.nz
email: cellar@matakanaestate.co.nz
facebook: https://www.facebook.com/Matakana-Estate-189311961089603/
twitter: https://twitter.com/matakanaestate
instagram: 
pintest:
---
{{< comment >}}description{{< /comment >}}
Matakana Estate is located in the heart of Matakana, a hidden gem of a wine region. Less than an hour north of Auckland this makes for a great weekend destination with green hills and beautiful sandy beaches right near by. Matakana also has vineyards in some of New Zealand's top wine regions so they have a wide variety of wines from premium selections of grapes.

{{< load-hours >}}
{{< load-phones >}}
{{< load-leaflet >}}
