---
title: Matavino Wines
tags: ["matavino wines","winery","auckland","matakana","cellar door"]
type: winery
lat: -36.4007862
long: 174.6950045
address: ["6 Hamilton Rd","Warkworth 0982, New Zealand"]
phone: 
  1: ["+64 021 621 124",""]
hours: 
  1: ["12:00pm to 5:00pm","Sat-Sun"]
  2: ["other times","by appointment"]
website: http://www.matavino.co.nz/
email: sales@matavino.co.nz
facebook: 
twitter: 
instagram: https://www.instagram.com/matavinowine/
pintest: 
---
{{< comment >}}description{{< /comment >}}
Matavino is a small vineyard nestled on northern slopes that epitomises the diversity of the famous Matakana Wine Region by growing the ancient Italian red grape varieties of Dolcetto, Barbera, and Nebbiolo, plus Chardonnay and Viognier.

{{< load-hours >}}
{{< load-phones >}}
{{< load-leaflet >}}
