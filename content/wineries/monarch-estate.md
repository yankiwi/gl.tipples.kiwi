---
title: Monarch Estate Vineyard
tags: ["monarch","winery","auckland","matakana","accommodations"]
type: winery
lat: -36.383220
long: 174.685610
address: ["150 Monarch Downs Way","Matakana","RD2 Warkworth","Auckland 0982, New Zealand"]
phone: 
  1: ["+64 021 863 463",""]
hours: 
  1: ["by appointment only"]
website: https://monarchestate.co.nz/
email: info@monarchestate.co.nz
facebook: https://www.facebook.com/pg/MonarchEstateVineyard/
twitter: 
instagram: 
pintest: 
---
{{< comment >}}description{{< /comment >}}
Located in Matakana, just 40 minutes north of Auckland, Monarch Estate Vineyard is a magnificent 20 acre property featuring a private vineyard on its sunny northern slopes.


{{< load-hours >}}
{{< load-phones >}}
{{< load-leaflet >}}
