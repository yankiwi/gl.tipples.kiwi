---
title: Omaha Bay Vineyard
tags: ["omaha bay vineyard","winery","auckland","cellar door"]
type: winery
lat: -36.344790
long: 174.732540
address: ["169 Takatu Road","Matakana","New Zealand"]
phone: 
  1: ["+64 09 423 0022",""]
hours: 
  1: ["11.00am to 5.00pm","Wed-Sun"]
website: http://www.omahabay.co.nz/
email: omahabayvineyardobv@gmail.com
facebook: https://www.facebook.com/Omaha-Bay-Vineyard-OBV-225961599538/
twitter: 
instagram: 
pintest: 
---
{{< comment >}}description{{< /comment >}}
Omaha Bay Vineyard is the vision of Hegman and Bev Foster, a family owned boutique Vineyard and Winery, located in the Matakana wine region, one hour north of Auckland City, overlooking Omaha Bay and Little Barrier Island. 

{{< load-hours >}}
{{< load-phones >}}
{{< load-leaflet >}}
