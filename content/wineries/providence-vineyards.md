---
title: Providence Vineyards
tags: ["providence vineyards","winery","auckland","matakana"]
type: winery
lat: -36.346930
long: 174.733760
address: ["45 Takatu Rd","Tawharanui Peninsula","Auckland, 0986, New Zealand"]
phone: 
  1: ["+64 09 422 9300",""]
hours: 
  1: ["by appointment only",""]
website: https://www.providencewines.com/
email: 
facebook: https://www.facebook.com/providencevineyards
twitter: https://twitter.com/ProvidenceWine
instagram: 
pintest: 
---
{{< comment >}}description{{< /comment >}}
Providence is located in the rolling hills of the Matakana region, 60 kilometres north of Auckland. 

The vineyard is situated between the Pacific Ocean to the east and the Rodney Ranges (standing at over 400 metres) 2 kilometres to the west. 

Due to these features, the area enjoys a microclimate, noted for low rainfall and high sunshine hours, ideal to produce the best red wine in New Zealand.


{{< load-hours >}}
{{< load-phones >}}
{{< load-leaflet >}}
