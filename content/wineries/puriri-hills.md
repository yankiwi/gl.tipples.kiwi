---
title: Puriri Hills
tags: ["puriri","winery","auckland","cellar door"]
type: winery
lat: -36.967236
long: 175.0155209
address: ["398 North Road","Clevedon, R.D. 2","Papakura, New Zealand"]
phone: 
  1: ["+64 09 292 9264"," "]
hours: 
  1: ["1.00 to 4.00pm","most weekends"]
  2: ["by appointment","during winter months"]
website: http://www.puririhills.com/
facebook: 
twitter: http://www.twitter.com/puririhills
instagram: 
pintest:
---
{{< comment >}}description{{< /comment >}}
Taking its name the native Puriri trees, Puriri Hills is located in the scenic hills near Clevedon, NZ, making it an easy trip from Auckland, the Coromandel or after a hike or mountain bike ride at the Hunua Ranges.  At Puriri Hills you'll find a luxurious selection of French inspired red blends.

{{< load-hours >}}
{{< load-phones >}}
{{< load-leaflet >}}
