---
title: Runner Duck Estate
tags: ["runner duck","restaurant","auckland","matakana","cellar door","accommodations"]
type: winery
lat: -36.376240
long: 174.704060
address: ["49A Sharp Rd, Matakana","Auckland 0982, New Zealand"]
phone: 
  1: ["+64 09 422 7915",""]
hours: 
  1: ["11.00am to 2.00pm","Wed-Fri"]
  2: ["12.00am to 3:00pm","Sat-Sun"]
website: https://runnerduck.co.nz/
email: 
facebook: 
twitter: 
instagram: 
pintest: 
---
{{< comment >}}description{{< /comment >}}
The 3 hectare boutique vineyard located at 37 Duck Creek Road in the Matakana appellation was planted in 2003. The vineyard comprises about 1.5 hectare of Syrah, with the remainder being in Cabernet Franc, Malbec, Merlot and Petit Verdot allowing Runner Duck Estate to produce a pure Bordeaux varietal, a Syrah, a methode tradittionale and a Rose.

<b>Runner Duck Estate’s Cellar Door is situated at [Plume Vineyard Restaurant](/other/plume-restaurant/) at: 49a Sharp Road, Matakana.</b>

{{< load-hours >}}
{{< load-phones >}}
{{< load-leaflet >}}

