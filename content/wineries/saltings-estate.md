---
title: Saltings Estate Vineyard
tags: ["saltings estate","winery","auckland","matakana","cellar door","accommodations"]
type: winery
lat: -36.398280
long: 174.685750
address: ["1210 Sandspit Road","Warkworth, New Zealand"]
phone: 
  1: ["+64 09 425 9670",""]
hours: 
  1: ["by appointment",""]
website: http://www.saltings.co.nz/
email: relax@saltings.co.nz
facebook: 
twitter: 
instagram: 
pintest: 
---
{{< comment >}}description{{< /comment >}}
Saltings Estate is a small boutique family owned vineyard of three and a half acres on the banks of the Sandspit Estuary, just north of Warkworth in the Matakana wine growing area.  Established in 2003, Salting produces classical style wines that express the qualities of our terroir,  that is our soils, microclimate, biodynamic management and care of the vineyard.

The Merlot, Malbec, Cabernet Franc and Syrah grapes are grown on warm, clay/loam north-facing slopes where the temperatures are moderated by coastal breezes.  The vineyard is tended using only biodynamic and organic methods and the grapes are hand-picked at harvest. 

{{< load-hours >}}
{{< load-phones >}}
{{< load-leaflet >}}
