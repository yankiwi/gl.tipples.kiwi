---
title: Takatu Lodge & Vineyard
tags: ["takatu","winery","auckland","matakana","cellar door","accommodations"]
type: winery
lat: -36.363170
long: 174.744750
address: ["518 Whitmore Road, RD6","Matakana, Auckland, New Zealand"]
phone: 
  1: ["+64 09 423 0299",""]
hours: 
  1: ["",""]
website: http://www.takatulodge.co.nz/
email: info@takatulodge.co.nz
facebook: https://www.facebook.com/takatulodgenz
twitter: 
instagram: 
pintest: 
---
{{< comment >}}description{{< /comment >}}
On a hill close to the small village of Matakana, sits Takatu Vineyard with its north facing view and sea breeze blowing from Omaha Bay, plus the gale from the South West to add the winter chill factor.

{{< load-hours >}}
{{< load-phones >}}
{{< load-leaflet >}}
