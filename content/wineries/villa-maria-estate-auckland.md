---
title: Villa Maria Estate (Auckland)
tags: ["villa maria","winery","auckland","cellar door","cafe"]
type: winery
lat: -36.9806513
long: 174.7757891
address: ["118 Montgomerie Road","Mangere, Manukau 2022","New Zealand"]
phone: 
  1: ["+64 09 255 0660",""]
hours: 
  1: ["9.00am to 6.00pm","Mon-Fri"]
  2: ["9.00am to 4:00pm","Sat"]
website: http://www.villamaria.co.nz/
facebook: https://facebook.com/villamariawine/
twitter: http://www.twitter.com/villamaria_wine/
instagram: https://www.instagram.comvillamariawines/
pintest: https://pinterest.nz/villamaria/
---
{{< comment >}}description{{< /comment >}}
Founded in 1961, Villa Maria has grown into New Zealand’s Most Awarded Winery. Villa Maria has a large selection of wine that is exported and enjoyed by wine lovers around the world.

Villa Maria's Auckland location is a beautiful vinyard just minutes from the Auckland Airport offering tours, cellar door tastings and a cafe.

{{< load-hours >}}
{{< load-phones >}}
{{< load-leaflet >}}
