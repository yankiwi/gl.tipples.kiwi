---
title: Villa Maria Estate (Marlborough)
tags: ["villa maria","winery","marlborough","cellar door"]
type: winery
lat: -41.497162
long: 173.783279
address: ["New Renwick Rd","Fairhall","Blenheim 7201"]
phone: 
  1: ["+64 3 520 8470",""]
hours: 
  1: ["10:00am to 5:00pm","Mon-Sun"]
  2: ["12:00pm to 5:00pm","weekends between Jun - Sep"]
website: http://www.villamaria.co.nz/
facebook: https://facebook.com/villamariawine/
twitter: http://www.twitter.com/villamaria_wine/
instagram: https://www.instagram.comvillamariawines/
pintest: https://pinterest.nz/villamaria/
---
{{< comment >}}description{{< /comment >}}
Founded in 1961, Villa Maria has grown into New Zealand’s Most Awarded Winery. Villa Maria has a large selection of wine that is exported and enjoyed by wine lovers around the world.

Villa Maria's Marlborough location is found amoung Blenheim's extensive vineyard area and just minutes from the from Blenheim Airport.

{{< load-hours >}}
{{< load-phones >}}
{{< load-leaflet >}}
